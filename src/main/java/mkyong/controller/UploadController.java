package mkyong.controller;

import mkyong.Repo.Client;
import mkyong.Repo.MongoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by Kalmurzayev on 29.04.17.
 */

@Controller
public class UploadController {
    @Autowired
    MongoRepo repo;
    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "/home/lordtemich/Документы/mid/";

    @GetMapping("/")
    public String index() {
        return "upload";
    }
    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }

    @GetMapping("/loginStatus")
    public String loginStatus() {
        return "loginStatus";
    }
    @GetMapping("/login")
    public String lis(){
        return "login";
    }
    @PostMapping("/login")
    public String listing(@RequestParam String login, RedirectAttributes redirectAttributes){
        try {
            String s;
            if(login.equals("spam")){
                ArrayList<Client> cl = repo.findBySpam(true);
                s="SPAM :  ";
                for (Client i : cl) {
                    s+=i.getInfo();
                }
            }
            else if(login.equals("no spam")){
                ArrayList<Client> cl = repo.findBySpam(false);
                s="";
                for (Client i : cl) {
                    s+=i.getInfo();
                }
            }
            else if(login.equals("delete spam")){
                s="ALL SPAM DELETED";
                ArrayList<Client> cl=repo.findBySpam(true);
                for(Client i : cl){
                    new File(i.getUrl()).delete();
                    repo.delete(i);
                }
            }
            else if(login.equals("delete all")){
                s="ALL FILES DELETED";

                repo.deleteAll();
            }
            redirectAttributes.addFlashAttribute("message",s);
        }
        catch (NullPointerException e){
            redirectAttributes.addFlashAttribute("message", "No user");
        }
        return "redirect:/loginStatus";
    }

    @PostMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {
        String fn=file.getOriginalFilename();
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload or do not put empty files");
            return "redirect:uploadStatus";
        }
        if(!fn.substring(fn.length()-3,fn.length()).equals("txt")){
            redirectAttributes.addFlashAttribute("message", "You can use only txt files");
            return "redirect:uploadStatus";
        }

        try {
            boolean spamin=false;
            String dir=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            Scanner in=new Scanner(file.getInputStream());
            while(in.hasNextLine()){
                String s=in.nextLine();
                if(s.indexOf("spam")!=-1){
                    spamin=true;
                }
                if(spamin){
                    break;
                }
            }
            if(new File(UPLOADED_FOLDER+dir).isDirectory()){

            }
            else{
                new File(UPLOADED_FOLDER+dir).mkdir();
            }
            if(spamin){
                byte[] bytes = file.getBytes();// hello.txt
                Client cl;
                if(new File(UPLOADED_FOLDER + "spam/" + file.getOriginalFilename()).isFile()){
                    Path path = Paths.get(UPLOADED_FOLDER + "spam/" + file.getOriginalFilename().substring(0,file.getOriginalFilename().length()-4)+" "+new Date()+".txt");
                    Files.write(path, bytes);
                    cl=new Client(UPLOADED_FOLDER + "spam/" + file.getOriginalFilename().substring(0,file.getOriginalFilename().length()-4)    +" "+new Date()+".txt");

                }
                else{
                    Path path = Paths.get(UPLOADED_FOLDER + "spam/" + file.getOriginalFilename());
                    Files.write(path, bytes);
                    cl=new Client(UPLOADED_FOLDER + "spam/" + file.getOriginalFilename());
                }

                redirectAttributes.addFlashAttribute("message",
                        "You successfully uploaded to SPAM'" + file.getOriginalFilename() + "'");

                cl.setSpam(true);
                repo.save(cl);
            }
            else {
                byte[] bytes = file.getBytes();
                Client cl;
                if(new File(UPLOADED_FOLDER + dir + "/" + file.getOriginalFilename()).isFile()){
                    Path path = Paths.get(UPLOADED_FOLDER + dir + "/" + file.getOriginalFilename().substring(0,file.getOriginalFilename().length()-4)+" "+new Date()+".txt");
                    Files.write(path, bytes);
                    cl=new Client(UPLOADED_FOLDER + dir + "/" + file.getOriginalFilename().substring(0,file.getOriginalFilename().length()-4)+" "+new Date()+".txt");
                }
                else{
                    Path path = Paths.get(UPLOADED_FOLDER + dir + "/" + file.getOriginalFilename());
                    Files.write(path, bytes);
                    cl=new Client(UPLOADED_FOLDER + dir + "/" + file.getOriginalFilename());
                }

                redirectAttributes.addFlashAttribute("message",
                        "You successfully uploaded '" + file.getOriginalFilename() + "'");

                repo.save(cl);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/uploadStatus";
    }



}