package mkyong.Repo;

import org.springframework.data.annotation.Id;

import java.text.SimpleDateFormat;
import java.util.Date;

//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;

/**
 * Created by Kalmurzayev on 29.04.17.
 */
public class Client {
    @Id
    private String id;
    private String url;
    private String date;
    private boolean spam;
    public Client(){
        spam=false;
    }
    public Client(String login){
        this.url=login;spam=false;this.date=new SimpleDateFormat("yyyy-MM-dd").format(new Date());;
    }
    public void setSpam(boolean s){
        spam=s;
    }
    public String getInfo(){
        String s="";
        if(spam){
            s="spam";
        }
        return id+" : '"+url+"'  -  '"+date+"' \n"+s+" ; ";
    }
    public String getUrl(){
        return url;
    }
}
