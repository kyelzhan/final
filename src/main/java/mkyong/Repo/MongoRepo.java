package mkyong.Repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.ArrayList;

/**
 * Created by Kalmurzayev on 29.04.17.
 */
public interface MongoRepo  extends MongoRepository<Client, Long>{
        public Client findById(String id);
        public Client findByUrl(String url);
        public ArrayList<Client> findBySpam(boolean f);

}
